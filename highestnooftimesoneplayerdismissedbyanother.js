const match = require('./matches.json');
const delivery = require('./delivery.json');

const getData = ((match, delivery) => {

    const data = delivery.reduce ((acc, val) => {
        if (val.dismissal_kind !== 'run out' && val.player_dismissed !== '') {
            if (!acc[val.batsman]) {
                acc[val.batsman] = {};
                acc[val.batsman][val.bowler] = 1;
            }else {
                if (!acc[val.batsman][val.bowler]) {
                    acc[val.batsman][val.bowler] = 1;
                }else {
                    acc[val.batsman][val.bowler]++;
                }
            }
        }
        return acc;
    },{})

    const output = Object.entries (data).reduce ((acc, val) => {
        acc[val[0]] = Object.fromEntries(Object.entries (val[1]).sort ((a, b) => b[1] - a[1]).slice (0, 1));
        return acc;
    },{})

    console.log(output);

})

getData (match, delivery);