const match = require ('./matches.json');
const delivery = require ('./delivery.json');

const getData = ((match, delivery) => {

    const data = match.reduce ((acc, val) => {
        if (val.winner === val.toss_winner) {
            if (!acc[val.winner]) {
                acc[val.winner] = 1;
            }else {
                acc[val.winner]++;
            }
        }
        return acc;
    }, {})
    console.log(data);
    
})

getData (match, delivery);