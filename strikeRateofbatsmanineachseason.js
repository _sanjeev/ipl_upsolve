const match = require('./matches.json');
const delivery = require('./delivery.json');

const getData = ((match, delivery) => {

    const result = delivery.map ((key) => {
        const res = match.find ((val) => {
            return key.match_id === val.id;
        })
        key['season'] = res.season;
        return key;
    })

    const res = result.reduce ((acc, val) => {
        if (!acc[val.batsman]) {
            acc[val.batsman] = {};
            acc[val.batsman][val.season] = [];
            acc[val.batsman][val.season][0] = 1;
            acc[val.batsman][val.season][1] = Number (val.total_runs);
        }else {
            if (!acc[val.batsman][val.season]) {
                acc[val.batsman][val.season] = [];
                acc[val.batsman][val.season][0] = 1;
                acc[val.batsman][val.season][1] = Number (val.total_runs);
            }else {
                acc[val.batsman][val.season][0]++;
                acc[val.batsman][val.season][1] += Number (val.total_runs);
            }
        }
        return acc;
    }, {})
    
    const output = Object.entries (res).reduce ((acc, val) => {
        acc[val[0]] = Object.entries (val[1]).reduce ((acc, val) => {
            let over = Math.floor(val[1][0] / 6);
            let ball = (val[1][0] % 6) * 0.1;
            let totalOver = over + ball;
            let totalRuns = val[1][1];
            let economy = (totalRuns / totalOver).toFixed (2);
            acc[val[0]] = economy;
            return acc;
        }, {})
        return acc;
    }, {})
    console.log(output);
})

getData (match, delivery);