const match = require ('./matches.json');
const delivery = require ('./delivery.json');

const getData = ((match, delivery) => {

    const out = delivery.map ((val) => {
        const res = match.find ((key) => {
            return key.id === val.match_id;
        })
        val['season'] = res.season;
        return val;
    })
    

    const data2016 = out.filter ((val) => {
        return val.season === '2016';
    })

    const data = data2016.reduce ((acc, val) => {
        if (!acc[val.batting_team]) {
            acc[val.batting_team] = Number(val.extra_runs);
        }else {
            acc[val.batting_team] += Number(val.extra_runs);
        }
        return acc;
    }, {})
    console.log(data);
})

getData (match, delivery);