const match = require ('./matches.json');
const delivery = require ('./delivery.json');

const getData = ((match, delivery) => {

    const out = delivery.map ((val) => {
        const res = match.find ((key) => {
            return key.id === val.match_id;
        })
        val['season'] = res.season;
        return val;
    })
    

    const data2015 = out.filter ((val) => {
        return val.season === '2015';
    })

    const output = data2015.reduce ((acc, val) => {
        if (!acc[val.bowler]) {
            acc[val.bowler] = [];
            acc[val.bowler][0] = 1;
            acc[val.bowler][1] = Number(val.total_runs);
        }else {
            acc[val.bowler][0]++;
            acc[val.bowler][1] += Number(val.total_runs);
        }
        return acc;
    }, {})
    
    const res = Object.entries (output).reduce ((acc, val) => {
        let over = Math.floor(val[1][0] / 6);
        let ball = (val[1][0] % 6) * 0.1;
        let totalOver = over + ball;
        let economy = (totalOver / val[1][1]).toFixed(2);
        acc[val[0]] = economy;
        return acc;
    }, {})
    

    const top10 = Object.fromEntries(Object.entries (res).sort ((a, b) => {
        return b[1] - a[1];
    }).slice (0, 10))
    console.log(top10);

})

getData (match, delivery);