const match = require('./matches.json');
const delivery = require('./delivery.json');

const getData = ((match, delivery) => {

    const data = match.reduce((acc, val) => {
        if (!acc[val.player_of_match]) {
            acc[val.player_of_match] = {};
            acc[val.player_of_match][val.season] = 1;
        } else {
            if (!acc[val.player_of_match][val.season]) {
                acc[val.player_of_match][val.season] = 1;
            } else {
                acc[val.player_of_match][val.season] += 1;
            }
        }
        return acc;
    }, {})

    const output = Object.entries (data).reduce ((acc, val) => {
        acc[val[0]] = Object.fromEntries(Object.entries (val[1]).sort ((a, b) => {
            return b[1] - a[1];
        }).slice (0, 1));
        return acc;
    }, {})

    console.log(output);

})

getData(match, delivery);